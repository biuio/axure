# 原型展示

#### 介绍

本仓库用于展示Axure原型案例


#### 案例
[01高保真时钟](https://biuio.gitee.io/axure/01高保真时钟)

[02水波图](https://biuio.gitee.io/axure/02水波图)

[03特效按钮](https://biuio.gitee.io/axure/03特效按钮)

[04阿里云2022产品图标库](https://biuio.gitee.io/axure/04阿里云2022产品图标库)

[05阿里云2022产品图标库](https://biuio.gitee.io/axure/05BI数据可视化管理平台)

[06Zondicons2017图标库（元件库版）](https://biuio.gitee.io/axure/06Zondicons2017图标库（元件库版）)

[07Azure_Public_Service_Icons_V6](https://biuio.gitee.io/axure/07Azure_Public_Service_Icons_V6)